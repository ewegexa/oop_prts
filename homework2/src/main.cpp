#include <algorithm>
#include <numeric>
#include <iostream>

#include "my_list.h"

void TestInsertAndErase() {
    std::cout << "Test Insert and Erase:" << std::endl;

    Homework::List<int> my_list(1, 1);

    for (auto p = my_list.begin(); p != my_list.end(); ++p)
    {
        std::cout << "first element: " << *p << std::endl;
    }

    my_list.insert(my_list.end(), 2);
    my_list.insert(my_list.end(), 3);
    my_list.insert(my_list.end(), 4);
    my_list.insert(my_list.end(), 5);

    my_list.insert(my_list.begin(), 0);

    std::cout << "past insert: ";
    for (auto p = my_list.begin(); p != my_list.end(); ++p)
    {
        std::cout << *p << " ";
    }
    std::cout << std::endl;

    my_list.erase(my_list.begin());
    my_list.erase(my_list.end() - 1);
    my_list.erase(my_list.begin() + 2);

    std::cout << "past erase: ";
    for (auto p = my_list.begin(); p != my_list.end(); ++p)
    {
        std::cout << *p << " ";
    }
    std::cout << std::endl;
}

void TestMove() {
    std::cout << "Test Move:" << std::endl;

    Homework::List<int> my_list(10, 1);
    std::iota(my_list.begin(), my_list.end(), 1);

    std::cout << "without move: ";
    for (auto p = my_list.begin(); p != my_list.end(); ++p)
    {
        std::cout << *p << " ";
    }
    std::cout << std::endl;

    my_list.swap(my_list.begin(), my_list.end() - 1);
    my_list.swap(my_list.begin() + 2, my_list.end() - 2);
    my_list.swap(my_list.begin() + 5, my_list.begin());

    std::cout << "past move: ";
    for (auto p = my_list.begin(); p != my_list.end(); ++p)
    {
        std::cout << *p << " ";
    }
    std::cout << std::endl;
}

void TestSort() {
    std::cout << "Test Sort:" << std::endl;

    Homework::List<int> my_list(10, 1);
    std::iota(my_list.begin(), my_list.end(), 1);
    std::random_shuffle(my_list.begin(), my_list.end());

    std::cout << "befor sort: ";
    for (auto p = my_list.begin(); p != my_list.end(); ++p)
    {
        std::cout << *p << " ";
    }
    std::cout << std::endl;

    my_list.sort(my_list.begin(), my_list.end());

    std::cout << "past sort: ";
    for (auto p = my_list.begin(); p != my_list.end(); ++p)
    {
        std::cout << *p << " ";
    }
    std::cout << std::endl;
}

int main() {
    TestInsertAndErase();
    TestMove();
    TestSort();
    return 0;
}