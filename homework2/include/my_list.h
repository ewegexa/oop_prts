#pragma once

#include <iostream>
#include <cstddef>

namespace Homework
{
    template <typename T>
    class List
    {
    private:
        struct Node
        {
            Node *prev_ = nullptr;
            Node *next_ = nullptr;
            T value_ = T();
        };

    public:
        class ListIterator
        {
            friend List;

        public:
            explicit ListIterator(Node *node);
            ListIterator(ListIterator const &iterator);
            ~ListIterator();
            ListIterator &operator=(ListIterator const & iterator);

            ListIterator &operator++();
            ListIterator &operator--();
            ListIterator operator+(int bias) const;
            ListIterator operator-(int bias) const;
            size_t operator-(ListIterator const &right) const;

            ListIterator const &operator++() const;
            ListIterator const &operator--() const;

            T &operator*();
            T *operator->();

            T const &operator*() const;
            T const *operator->() const;

            bool operator!=(ListIterator right) const;
            bool operator==(ListIterator right) const;

            Node const *get() const;

        private:
            Node** current_node_;
        };

    public:
        typedef T value_type;
        typedef ListIterator Iterator;
        typedef const ListIterator ConstIterator;

    public:
        explicit List(size_t count, T const &value = T());
        List();
        ~List();

        ListIterator begin();
        ListIterator end();
        ListIterator const cbegin() const;
        ListIterator const cend() const;

        size_t size() const;

        ListIterator insert(ListIterator const& pos, T const &value);
        ListIterator erase(ListIterator const& pos);
        bool swap(ListIterator pos1, ListIterator pos2);
        void sort(ListIterator begin, ListIterator end);

    private:
        size_t size_;
        Node* head_;
        Node* end_;
    };

    template <typename T>
    List<T>::List(size_t count, T const &value)
    {
        Node* end_node = new Node();
        head_ = end_node;
        size_ = 0;

        for (size_t i = 0; i < count; ++i)
        {
            Node *new_node = new Node();

            end_node->next_ = new_node;
            new_node->prev_ = end_node;

            end_node->value_ = value;

            ++size_;
            end_node = new_node;
        }
        end_ = end_node;
    }

    template <typename T>
    List<T>::List()
    {
        end_ = new Node();
        head_ = end_;
        size_ = 0;
    }

    template <typename T>
    List<T>::~List()
    {
        while (head_ != end_)
        {
            Node *new_end = end_->prev_;
            delete end_;
            end_ = new_end;
        }

        delete head_;
    }

    template <typename T>
    typename List<T>::ListIterator List<T>::begin()
    {
        return ListIterator(head_);
    }

    template <typename T>
    typename List<T>::ListIterator List<T>::end()
    {
        return ListIterator(end_);
    }

    template <typename T>
    typename List<T>::ListIterator const List<T>::cbegin() const
    {
        return ListIterator(head_);
    }

    template <typename T>
    typename List<T>::ListIterator const List<T>::cend() const
    {
        return ListIterator(end_);
    }

    template <typename T>
    typename List<T>::ListIterator List<T>::insert(List<T>::ListIterator const& pos, T const &value)
    {
        Node *new_node = new Node();
        new_node->value_ = value;
        new_node->next_ = (*pos.current_node_);
        new_node->prev_ = (*pos.current_node_)->prev_;
        (*pos.current_node_)->prev_ = new_node;
        if (head_ == (*pos.current_node_))
        {
            head_ = new_node;
        } else {
            new_node->prev_->next_ = new_node;
        }
        ++size_;
        return ListIterator(new_node);
    }

    template <typename T>
    typename List<T>::ListIterator List<T>::erase(List<T>::ListIterator const& pos)
    {
        if ((*pos.current_node_) == end_)
        {
            return ListIterator(end_);
        }
        else
        {
            if((*pos.current_node_) != head_)
            {
                (*pos.current_node_)->prev_->next_ = (*pos.current_node_)->next_;
                (*pos.current_node_)->next_->prev_ = (*pos.current_node_)->prev_;
            }
            else
            {
                head_ = (*pos.current_node_)->next_;
                (*pos.current_node_)->next_->prev_ = nullptr;
            }
            ListIterator rezalt((*pos.current_node_)->next_);
            delete (*pos.current_node_);

            --size_;
            return rezalt;
        }
    }
    // Lasciate ogni speranza, voi ch'entrate
    template <typename T>
    bool List<T>::swap(List<T>::ListIterator pos1,
                       List<T>::ListIterator pos2)
    {
        if(pos1.get() == end_ || pos2.get() == end_)
        {
            return false;
        }

        if(pos1.get() == pos2.get())
        {
            return true;
        }

        if(pos1.get() == head_)
        {
            head_ = (*pos2.current_node_);
        }
        else if(pos2.get() == head_)
        {
            head_ = (*pos1.current_node_);
        }

        if ((*pos1.current_node_)->prev_ == (*pos2.current_node_))
        {
            if ((*pos2.current_node_)->prev_ != nullptr)
            {
                (*pos2.current_node_)->prev_->next_ = *pos1.current_node_;
            }
            (*pos1.current_node_)->next_->prev_ = *pos2.current_node_;

            Node *buf_next_pos1 = (*pos1.current_node_)->next_;
            Node *buf_prev_pos2 = (*pos2.current_node_)->prev_;

            (*pos1.current_node_)->next_ = *pos2.current_node_;
            (*pos2.current_node_)->prev_ = *pos1.current_node_;

            (*pos1.current_node_)->prev_ = buf_prev_pos2;
            (*pos2.current_node_)->next_ = buf_next_pos1;
        }
        else if ((*pos1.current_node_)->next_ == *pos2.current_node_)
        {
            if ((*pos1.current_node_)->prev_ != nullptr)
            {
                (*pos1.current_node_)->prev_->next_ = *pos2.current_node_;
            }
            (*pos2.current_node_)->next_->prev_ = *pos1.current_node_;

            Node *buf_prev_pos1 = (*pos1.current_node_)->prev_;
            Node *buf_next_pos2 = (*pos2.current_node_)->next_;

            (*pos1.current_node_)->prev_ = *pos2.current_node_;
            (*pos2.current_node_)->next_ = *pos1.current_node_;

            (*pos1.current_node_)->next_ = buf_next_pos2;
            (*pos2.current_node_)->prev_ = buf_prev_pos1;
        }
        else
        {
            Node *buf_prev_pos1 = (*pos1.current_node_)->prev_;
            Node *buf_next_pos1 = (*pos1.current_node_)->next_;

            Node *buf_prev_pos2 = (*pos2.current_node_)->prev_;
            Node *buf_next_pos2 = (*pos2.current_node_)->next_;

            if (buf_prev_pos1 != nullptr)
            {
                buf_prev_pos1->next_ = *pos2.current_node_;
            }

            (*pos2.current_node_)->prev_ = buf_prev_pos1;

            if (buf_prev_pos2 != nullptr)
            {
                buf_prev_pos2->next_ = *pos1.current_node_;
            }

            (*pos1.current_node_)->prev_ = buf_prev_pos2;

            buf_next_pos1->prev_ = *pos2.current_node_;
            (*pos2.current_node_)->next_ = buf_next_pos1;

            buf_next_pos2->prev_ = *pos1.current_node_;
            (*pos1.current_node_)->next_ = buf_next_pos2;
        }

        return true;
    }
    // Lasciate ogni speranza, voi ch'entrate
    template <typename T>
    void List<T>::sort(typename List<T>::ListIterator begin, typename List<T>::ListIterator end)
    {
        if(begin == end || (begin + 1) == end)
        {
            return;
        }

        if((begin + 2) == end)
        {
            if (*(begin + 1) < *begin)
            {
                swap(begin, (begin + 1));
            }
            return;
        }

        ListIterator standart = begin;
        bool is_first_move = false;
        for (ListIterator p = begin + 1; p != end;)
        {
            if(*p < *standart)
            {
                if(p == (standart + 1))
                {
                    swap(p, standart);
                    if (!is_first_move)
                    {
                        begin = standart - 1;
                        is_first_move = true;
                    }
                    p = standart + 1;
                }
                else
                {
                    ListIterator buf = p + 1;
                    swap(standart, (standart + 1));
                    swap((standart - 1), p);
                    if (!is_first_move)
                    {
                        begin = standart - 1;
                        is_first_move = true;
                    }
                    p = buf;
                }
            } else
            {
                ++p;
            }
        }
        sort(begin, standart);
        sort(standart + 1, end);
    }

    template <typename T>
    size_t List<T>::size() const
    {
        return size_;
    }

    template <typename T>
    List<T>::ListIterator::ListIterator(Node *node) : current_node_(new Node*)
    {
        *current_node_ = node;
    }

    template <typename T>
    List<T>::ListIterator::ListIterator(List<T>::ListIterator const &iterator)
    {
        current_node_ = new Node *;
        *current_node_ = *iterator.current_node_;
    }

    template <typename T>
    List<T>::ListIterator::~ListIterator()
    {
        delete current_node_;
    }

    template <typename T>
    typename List<T>::ListIterator &List<T>::ListIterator::operator=(List<T>::ListIterator const &iterator)
    {
        if(&iterator != this)
        {
            delete current_node_;
            current_node_ = new Node*;
            *current_node_ = *iterator.current_node_;
        }
        return *this;
    }

    template <typename T>
    typename List<T>::ListIterator &List<T>::ListIterator::operator++()
    {
        *current_node_ = (*current_node_)->next_;
        return *this;
    }

    template <typename T>
    typename List<T>::ListIterator &List<T>::ListIterator::operator--()
    {
        *current_node_ = (*current_node_)->prev_;
        return *this;
    }

    template <typename T>
    typename List<T>::ListIterator const &List<T>::ListIterator::operator++() const
    {
        *current_node_ = (*current_node_)->next_;
        return *this;
    }

    template <typename T>
    typename List<T>::ListIterator const &List<T>::ListIterator::operator--() const
    {
        *current_node_ = (*current_node_)->prev_;
        return *this;
    }

    template <typename T>
    typename List<T>::ListIterator List<T>::ListIterator::operator+(int bias) const
    {
        if (bias < 0)
        {
            bias *= -1;
            return *this - bias;
        }

        List<T>::ListIterator rezalt(*this);
        while (bias > 0 && (*current_node_)->next_ != nullptr)
        {
            ++rezalt;
            --bias;
        }
        return rezalt;
    }

    template <typename T>
    typename List<T>::ListIterator List<T>::ListIterator::operator-(int bias) const
    {
        if (bias < 0)
        {
            bias *= -1;
            return *this + bias;
        }

        List<T>::ListIterator rezalt(*this);
        while (bias > 0 && (*current_node_)->prev_ != nullptr)
        {
            --rezalt;
            --bias;
        }
        return rezalt;
    }

    template <typename T>
    size_t List<T>::ListIterator::operator-(typename List<T>::ListIterator const &right) const
    {
        size_t rezalt = 0;
        ListIterator copy_this_iterator(*this);
        while (copy_this_iterator != right)
        {
            --copy_this_iterator;
            ++rezalt;
        }
        return rezalt;
    }

    template <typename T>
    T const &List<T>::ListIterator::operator*() const
    {
        return (*current_node_)->value_;
    }

    template <typename T>
    T const *List<T>::ListIterator::operator->() const
    {
        return &((*current_node_)->value_);
    }

    template <typename T>
    T &List<T>::ListIterator::operator*()
    {
        return (*current_node_)->value_;
    }

    template <typename T>
    T *List<T>::ListIterator::operator->()
    {
        return &((*current_node_)->value_);
    }

    template <typename T>
    bool List<T>::ListIterator::operator!=(typename List<T>::ListIterator right) const
    {
        return *current_node_ != right.get();
    }

    template <typename T>
    bool List<T>::ListIterator::operator==(typename List<T>::ListIterator right) const
    {
        return *current_node_ == right.get();
    }

    template <typename T>
    typename List<T>::Node const *List<T>::ListIterator::get() const
    {
        return *current_node_;
    }
}