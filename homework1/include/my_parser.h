#pragma once

#include <cstddef>

namespace Homework
{
    float big2little(float f);

    struct PNI_Trax_Data
    {
        float kHeading = 0;
        float kPitch = 0;
        float kRoll = 0;
        char kHeadingStatus = 0;
        float kQuaternion[4] = {0, 0, 0, 0};
        float kTemperature = 0;
        bool kDistortion = false;
        bool kCalStatus = false;
        float kAccelX = 0;
        float kAccelY = 0;
        float kAccelZ = 0;
        float kMagX = 0;
        float kMagY = 0;
        float kMagZ = 0;
        float kGyroX = 0;
        float kGyroY = 0;
        float kGyroZ = 0;
    };

    class Parser_PNI_Trax
    {
    public:
        void Update_Data(char *message);
        void Update_Data(char *message, size_t size);

        PNI_Trax_Data Get_Data() const;
        void Print_Data() const;

    private:
        size_t Pars_One_ID(char *message, size_t size);
        float Get_FLoat(char *src);

    private:
        PNI_Trax_Data data_;
    };

} // end of namecpace Homework