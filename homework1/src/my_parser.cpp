#include <cstring>
#include <iostream>

#include "my_parser.h"

using namespace Homework;

float Homework::big2little(float f)
{
    union
    {
        float f;
        char b[4];
    } src, dst;

    src.f = f;
    dst.b[3] = src.b[0];
    dst.b[2] = src.b[1];
    dst.b[1] = src.b[2];
    dst.b[0] = src.b[3];
    return dst.f;
}

void Parser_PNI_Trax::Update_Data(char *message)
{
    Update_Data(message, std::strlen(message));
}

void Parser_PNI_Trax::Update_Data(char *message, size_t size)
{
    for (size_t i = 0; i < size;)
    {
        i = Pars_One_ID(message, i);
    }
}

size_t Parser_PNI_Trax::Pars_One_ID(char *message, size_t size)
{
    switch (message[size])
    {
        case 5:
        {
            data_.kHeading = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 24:
        {
            data_.kPitch = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 25:
        {
            data_.kRoll = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 79:
        {
            data_.kHeadingStatus = message[size + 1];
            size += 2;
            break;
        }
        case 77:
        {
            for (size_t i = 0; i < 4; ++i) {
                data_.kQuaternion[i] = Get_FLoat(message + size + 1 + 4 * i);
            }

            size += 17;
            break;
        }
        case 7:
        {
            data_.kTemperature = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 8:
        {
            data_.kDistortion = static_cast<bool>(message[size + 1]);
            size += 2;
            break;
        }
        case 9:
        {
            data_.kCalStatus = static_cast<bool>(message[size + 1]);
            size += 2;
            break;
        }
        case 21:
        {
            data_.kAccelX = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 22:
        {
            data_.kAccelY = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 23:
        {
            data_.kAccelZ = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 27:
        {
            data_.kMagX = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 28:
        {
            data_.kMagY = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 29:
        {
            data_.kMagZ = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 74:
        {
            data_.kGyroX = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 75:
        {
            data_.kGyroY = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        case 76:
        {
            data_.kGyroZ = Get_FLoat(message + size + 1);
            size += 5;
            break;
        }
        default:
        {
            throw "Incorrect ID";
            break;
        }
    }

    return size;
}

float Parser_PNI_Trax::Get_FLoat(char *src)
{
    float new_float = 0;
    std::memcpy(&new_float, src, sizeof(float));
    return big2little(new_float);
}

PNI_Trax_Data Parser_PNI_Trax::Get_Data() const
{
    return data_;
}

void Parser_PNI_Trax::Print_Data() const
{
    std::cout << "Heading: "        << data_.kHeading       << std::endl
              << "Pitch: "          << data_.kPitch         << std::endl
              << "Roll: "           << data_.kRoll          << std::endl
              << "Heading Status: " << static_cast<int>(data_.kHeadingStatus) << std::endl
              << "Qternion X: "     << data_.kQuaternion[0] << std::endl
              << "Qternion Y: "     << data_.kQuaternion[1] << std::endl
              << "Qternion Z: "     << data_.kQuaternion[2] << std::endl
              << "Qternion W: "     << data_.kQuaternion[3] << std::endl
              << "Temperature: "    << data_.kTemperature   << std::endl
              << "Distortion: "  << std::boolalpha << data_.kDistortion    << std::endl
              << "Cal. status: " << std::boolalpha << data_.kCalStatus     << std::endl
              << "Accel X: "        << data_.kAccelX        << std::endl
              << "Accel Y: "        << data_.kAccelY        << std::endl
              << "Accel Z: "        << data_.kAccelZ        << std::endl
              << "Mag X: "          << data_.kMagX          << std::endl
              << "Mag Y: "          << data_.kMagY          << std::endl
              << "Mag Z: "          << data_.kMagZ          << std::endl
              << "Gyro X: "         << data_.kGyroX         << std::endl
              << "Gyro Y: "         << data_.kGyroY         << std::endl
              << "Gyro Z: "         << data_.kGyroZ         << std::endl;
}