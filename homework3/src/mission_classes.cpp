#include <iostream>
#include <stdexcept>
#include <string>

#include "mission_classes.h"

using namespace std;

Dive::Dive(TypeSensors type_sensor, int depth, TypeDepth type_depth):
    type_sensor_(type_sensor),
    depth_(depth),
    type_depth_(type_depth)
{
    if(depth > DEPTH) {
        if(type_sensor == TypeSensors::PressureMeter)
        {
            throw invalid_argument("требуемая глубина погружения превышает максимальную глубину акватории"s);
        }
        else
        {
            throw invalid_argument("требуемое отстояние превышает поверзность акватории"s);
        }
    }
    else if(depth < 0)
    {
        throw invalid_argument("требуется ввести положительное число"s);
    }
}

std::ostream& Dive::Print(std::ostream& os) const
{
    os << "Погружение" << endl;
    if(type_sensor_ == TypeSensors::PressureMeter)
    {
        os << "Глубина погружения: ";
    }
    else
    {
        os << "Отстояние от дна: ";
    }

    os << depth_ << endl;

    if(type_depth_ == TypeDepth::Spiral)
    {
        os << "Погружение по спирали";
    }
    else
    {
        os << "Погружение за счет вертикальных двигателей";
    }

    return os;
}

bool Dive::IsEnd() const {
    return false;
}

Lift::Lift(TypeSensors type_sensor, int depth, TypeDepth type_depth):
    type_sensor_(type_sensor),
    depth_(depth),
    type_depth_(type_depth)
{
    if(depth > DEPTH) {
        if(type_sensor == TypeSensors::PressureMeter)
        {
            throw invalid_argument("требуемая глубина всплытия превышает поверхность акватории"s);
        }
        else
        {
            throw invalid_argument("требуемое отстояние превышает поверхность акватории"s);
        }
    }
    else if(depth < 0)
    {
        throw invalid_argument("требуется ввести положительное число"s);
    }
}

std::ostream& Lift::Print(std::ostream& os) const {
    os << "Всплытие" << endl;
    if(type_sensor_ == TypeSensors::PressureMeter)
    {
        os << "Глубина погружения: ";
    }
    else
    {
        os << "Отстояние от дна: ";
    }

    os << depth_ << endl;

    if(type_depth_ == TypeDepth::Spiral)
    {
        os << "Всплытие по спирали";
    }
    else
    {
        os << "Всплытие за счет вертикальных двигателей";
    }

    return os;
}

bool Lift::IsEnd() const {
    return false;
}

Move::Move(int x, int y, TypeOut type_out, TypeIn type_in, double radius):
    mv_coordinate_(x, y),
    type_out_(type_out),
    type_in_(type_in),
    radius_(radius)
{
    if(mv_coordinate_.first > WIDTH || mv_coordinate_.first < 0)
    {
        throw invalid_argument("требуемое перемещение по оси x выходит за пределы ширины акватории"s);
    }

    if(mv_coordinate_.second > LENGTH || mv_coordinate_.second < 0)
    {
        throw invalid_argument("требуемое перемещение по оси y выходит за пределы длины акватории"s);
    }
}

std::ostream& Move::Print(std::ostream& os) const
{
    os << "Перемещение" << endl;
    os << "Переместиться в координату X: " << mv_coordinate_.first << endl;
    os << "Переместиться в координату Y: " << mv_coordinate_.second << endl;

    if(type_in_ == TypeIn::AlongLine)
    {
        os << "Выход вдоль прямой" << endl;
    }
    else
    {
        os << "Выход в точку" << endl;
    }

    if(type_out_ == TypeOut::ConstDepth)
    {
        os << "Выход с сохранением глубины" << endl;
    }
    else
    {
        os << "Выход на постоянном отстоянии от дна" << endl;
    }

    os << "Радиус окрестности точки равен: " << radius_;
    return os;
}

bool Move::IsEnd() const {
    return false;
}

std::ostream& Return::Print(std::ostream& os) const {
    os << "Возврат на базу";
    return os;
}

bool Return::IsEnd() const {
    return true;
}
