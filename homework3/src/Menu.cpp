#include "Menu.h"

#include <iostream>
#include <fstream>
#include <cstdint>

using namespace std;

void Menu::Start()
{
    cout << "Вас привествует планировщик маршрутов версия: "s + VERSION << endl;
    cout << "Все координаты необходимо вводить относительно абсолютной системы кординат" << endl;
    cout << "Возврат в середине миссии с последующим ее продолжением счтитается валидной операцией" << endl;
    cout << "Считается, что абсолютная система координат находится в точке {0, 0, 0}" << endl;

    bool flag = true;
    while(flag) {
        cout << "Вы можете задать начальные координаты миссии (Глубина - положительное число): "<< endl;
        cout << "1 - Задать" << endl;
        cout << "2 - Использовать по умолчанию (X = 0, Y = 0, H = 0)" << endl;

        int number = 0;
        cin >> number;

        switch(number)
        {
            case 1:
                int64_t coordinate[3];

                cout << "X: ";
                cin >> coordinate[0];

                cout << "Y: ";
                cin >> coordinate[1];

                cout << "H: ";
                cin >> coordinate[2];

                if(coordinate[0] >= 0 && coordinate[0] <= WIDTH &&
                   coordinate[1] >= 0 && coordinate[1] <= LENGTH &&
                   coordinate[2] >= 0 && coordinate[2] <= DEPTH)
                {
                    coordinates_start_[0] = coordinate[0];
                    coordinates_start_[1] = coordinate[1];
                    coordinates_start_[2] = coordinate[2];

                    flag = false;
                }
                else
                {
                    cout << "Ошибка ввода" << endl;
                }
                break;
            case 2:
                flag = false;
                break;
            default:
                cout << "Ошибка ввода" << endl;
                break;
        }
    }

    while(number_methods_ != 0)
    {
        switch(number_methods_)
        {
            case 1:
                DefaultMenu();
                break;
            case 2:
                AddDive();
                break;
            case 3:
                AddLift();
                break;
            case 4:
                AddMove();
                break;
            case 5:
                AddReturn();
                break;
            case 6:
                Print();
                break;
            case 7:
                EndMenu();
                break;
            case 8:
                EndMenu();
                break;
            case 9:
                ClearMission();
                break;
            case 10:
                EditMenu();
                break;
            case 11:
                RmNode();
                break;
            case 12:
                InsertNode();
                break;
            case 13:
                MoveNode();
                break;
            case 14:
                ReplaceNode();
                break;
            default:
                cout << "Ошибка ввода" << endl;
                break;
        }
    }

    Safe();
}

void Menu::DefaultMenu()
{
    cout << endl;

    cout << "Меню действий" << endl;
    cout << "Выберете действие:" << endl;
    cout << "1 - Добавить к миссии погружение" << endl;
    cout << "2 - Добавить к миссии всплытие" << endl;
    cout << "3 - Добавить к миссии перемещение" << endl;
    cout << "4 - Завершить миссию возвращением на базу" << endl;
    cout << "5 - Показать миссию" << endl;
    cout << "6 - Завршить cоздание миссии" << endl;
    cout << "7 - Редактировать миссию" << endl;

    int number;
    cin >> number;

    if(number < 7 && number > 0)
    {
        number_methods_ = number + 1;
    }
    else if(number == 7)
    {
        number_methods_ = 10;
    }
    else
    {
        cout << "Неправильный ввод" << endl;
    }
}

Mission* Menu::AddVerticalMove(int dive_ar_lift)
{
    cout << endl;

    cout << "Выберите отстояние от дна или глубина" << endl;
    cout << "1 - Отстояние от дна" << endl;
    cout << "2 - Глубина" << endl;

    int number = 0;
    cin >> number;

    TypeSensors type_sensors;
    switch(number)
    {
        case 1:
            type_sensors = TypeSensors::EchoSounder;
            break;
        case 2:
            type_sensors = TypeSensors::PressureMeter;
            break;
        default:
            cout << "Ошибка ввода" << endl;
            return nullptr;
    }

    if(type_sensors == TypeSensors::EchoSounder)
    {
        cout << "Введите отстояние (значение должно быть положительным): ";
    }
    else
    {
        cout << "Введите гулбину (значение должно быть положительным): ";
    }

    unsigned int depth = 0;
    cin >> depth;

    cout << "Введите тип погружения:" << endl;
    cout << "1 - По спирали" << endl;
    cout << "2 - За счет веритикальных движителей" << endl;

    cin >> number;
    TypeDepth type_depth;
    switch(number)
    {
        case 1:
            type_depth = TypeDepth::Spiral;
            break;
        case 2:
            type_depth = TypeDepth::Vertical;
            break;
        default:
            cout << "Ошибка ввода" << endl;
            return nullptr;
    }

    switch(dive_ar_lift)
    {
        case 1:
            try{
                return new Dive(type_sensors, depth, type_depth);
            } catch(std::invalid_argument const& e) {
                cout << "Ошибка: " << e.what() << endl;
                return nullptr;
            }
        case 2:
            try{
                return new Lift(type_sensors, depth, type_depth);
            } catch(std::invalid_argument const& e) {
                cout << "Ошибка: " << e.what() << endl;
                return nullptr;
            }
        default:
            cout << "Неправильно задан тип возвращаемого значения в AddVerticalMove" << endl;
            return nullptr;
    }
}

void Menu::AddDive(int position) {
    ConfirmAdd(AddVerticalMove(1), "погружение"s, position);
}

void Menu::AddLift(int position) {
    ConfirmAdd(AddVerticalMove(2), "всплытие"s, position);
}

void Menu::AddMove(int position) {
    cout << endl;

    cout << "Введите координату x: ";

    int x = 0;
    cin >> x;

    cout << "Введите координату y: ";

    int y = 0;
    cin >> y;

    TypeIn type_in;
    cout << "Выберите способ выхода в точку:" << endl;
    cout << "1 - Просто выход в точку" << endl;
    cout << "2 - Движение вдоль прямой" << endl;

    int number = 0;
    cin >> number;

    switch(number)
    {
        case 1:
            type_in = TypeIn::InPoint;
            break;
        case 2:
            type_in = TypeIn::AlongLine;
            break;
        default:
            cout << "Ошибка ввода" << endl;
            number_methods_ = 1;
            return;
    }

    TypeOut type_out;
    cout << "Выберите способ выхода из точки:" << endl;
    cout << "1 - На постоянной глубине" << endl;
    cout << "2 - На постоянном отстоянии от дна" << endl;

    cin >> number;

    switch(number)
    {
        case 1:
            type_out = TypeOut::ConstDepth;
            break;
        case 2:
            type_out = TypeOut::ConstDistance;
            break;
        default:
            cout << "Ошибка ввода" << endl;
            number_methods_ = 1;
            return;
    }

    double precision = 0.5;

    bool flag = true;

    while(flag)
    {
        cout << "Желаете ввести точность?" << endl;
        cout << "1 - Ввести (не более 2 м)" << endl;
        cout << "2 - Использовать по умолчанию (" << precision << " м)" << endl;

        cin >> number;

        switch (number) {
            case 1:
                cin >> precision;
                if (precision > 2) {
                    cout << "Cлишком низкая точность" << endl;
                    precision = 0.5;
                }
                else
                {
                    flag = false;
                }
                break;
            case 2:
                flag = false;
                break;
            default:
                cout << "Ошибка ввода" << endl;
                break;
        }
    }

    ConfirmAdd(new Move(x, y, type_out, type_in, precision), "перемещение", position);
}

void Menu::ConfirmAdd(Mission* new_node, const string& type_new_node, int position)
{
    if(new_node != nullptr) {
        cout << "Добавить " << type_new_node << " к миссии?" << endl;
        cout << "1 - Добавить" << endl;
        cout << "2 - Вернуться к редактированию" << endl;

        int number;
        cin >> number;

        switch(number)
        {
            case 1:
                if(position == -1)
                {
                    list_mission_.PushBack(new_node);
                }
                else
                {
                    try
                    {
                        list_mission_.Insert(new_node, position);
                    }
                    catch(std::invalid_argument const& e)
                    {
                        cout << e.what();
                    }
                }
                number_methods_ = 1;
                return;
            case 2:
                delete(new_node);
                if("погружение"s == type_new_node)
                {
                    number_methods_ = 2;
                }
                else if("всплытие"s == type_new_node)
                {
                    number_methods_ = 3;
                }
                else
                {
                    number_methods_ = 4;
                }
                return;
            default:
                cout << "Ошибка ввода, " << type_new_node << " сброшено" << endl;
                delete(new_node);
                number_methods_ = 1;
                return;
        }
    }

    number_methods_ = 1;
}

void Menu::AddReturn(int position) {
    cout << endl;

    cout << "добавлена операция возвращения в точку старта" << endl;

    if(position == -1)
    {
        list_mission_.PushBack(new Return);
    }
    else
    {
        try
        {
            list_mission_.Insert(new Return, position);
        }
        catch(std::invalid_argument const& e)
        {
            cout << e.what();
        }
    }

    number_methods_ = 1;
}

void Menu::Print(bool return_menu, std::ostream& os)
{
    os << endl;

    os << "Начало мисси в точке с координатами: ("
         << coordinates_start_[0] << ", "
         << coordinates_start_[1] << ", "
         << coordinates_start_[2] << ")" << endl;

    int number = 1;

    for(auto ptr_mission : list_mission_.GetItems())
    {
        os << endl;
        os << number++ << endl;
        ptr_mission->Print(os) << endl;
    }

    if(return_menu)
    {
        number_methods_ = 1;
    }
}

void Menu::EndMenu()
{
    cout << endl;

    if(list_mission_.GetItems().size() == 0)
    {
        cout << "Мисия пустая" << endl;
        number_methods_ = 1;
        return;
    }

    if(!list_mission_.GetItems().back()->IsEnd())
    {
        cout << "Миссия должна заканчиваться возвращением в начальную точку" << endl;
        cout << "Добавить в миссию возвращение на базу?" << endl;
        cout << "1 - Да" << endl;
        cout << "2 - Нет, вернуться в меню формированая мисси" << endl;

        int number = 0;
        cin >> number;

        switch(number)
        {
            case 1:
                list_mission_.PushBack(new Return);
                break;
            case 2:
                number_methods_ = 1;
                return;
            default:
                cout << "Ошибка ввода" << endl;
                number_methods_ = 1;
                return;
        }
    }

    if(list_mission_.GetItems().size() == 1)
    {
        cout << "В миссии только возвращение на базу" << endl;
    }


    cout << "Завершение формирования миссии" << endl;
    cout << "1 - Заврешить формирование и сохранить в файл" << endl;
    cout << "2 - Показать миссию" << endl;
    cout << "3 - Удалить сформированную миссию и начать формирование с начала" << endl;
    cout << "4 - Редактировать миссию" << endl;

    int number = 0;
    cin >> number;

    switch(number)
    {
        case 1:
            number_methods_ = 0;
            break;
        case 2:
            Print(false, cout);
            break;
        case 3:
            number_methods_ = 9;
            break;
        case 4:
            number_methods_ = 10;
            break;
        default:
            cout << "Ошибка ввода" << endl;
            break;
    }
}

void Menu::ClearMission()
{
    list_mission_.Clear();

    number_methods_ = 1;
}

void Menu::EditMenu()
{
    cout << endl;

    cout << "Меню редактирования миссии" << endl;
    cout << "1 - Удалить задачу" << endl;
    cout << "2 - Вставить задачу" << endl;
    cout << "3 - Изменить порядок задач" << endl;
    cout << "4 - Изменить уже существующую задачу" << endl;
    cout << "5 - Записать в файл уже сформированнаы задачи " << endl;
    cout << "6 - Показать миссию" << endl;
    cout << "7 - Перейти в меню действий" << endl;

    int number = 0;
    cin >> number;

    switch(number)
    {
        case 1:
            number_methods_ = 11;
            break;
        case 2:
            number_methods_ = 12;
            break;
        case 3:
            number_methods_ = 13;
            break;
        case 4:
            number_methods_ = 14;
            break;
        case 5:
            Safe();
            number_methods_ = 10;
            break;
        case 6:
            Print(false);
            break;
        case 7:
            number_methods_ = 1;
            break;
        default:
            cout << "Ошибка ввода" << endl;
            number_methods_ = 11;
            break;
    }
}

void Menu::RmNode(int position)
{
    if(list_mission_.GetItems().empty())
    {
        return;
    }

    if(position == -1)
    {
        cout << endl;

        cout << "Введите номер удаляемой задачи: ";
        cin >> position;
    }

    if(position > list_mission_.GetItems().size() || position < 1)
    {
        cout << endl;

        cout << "В миссии нет задачи с таким номером" << endl;
    }
    else
    {
        list_mission_.Erase(position - 1);
    }

    number_methods_ = 10;
}

void Menu::InsertNode(int position)
{
    cout << endl;

    if(position == -1)
    {
        cout << "Введите номер вставляемой задачи: ";
        cin >> position;
    }

    if(position > (list_mission_.GetItems().size() + 1) || position < 1)
    {
        cout << "Последнее доступное место вставки - после последней задачи" << endl;
        cout << "Первое - перед первой задачей" << endl;
        number_methods_ = 10;
        return;
    }

    cout << "Выбирите тип вставляемой задачи:" << endl;
    cout << "1 - Добавить к миссии погружение" << endl;
    cout << "2 - Добавить к миссии всплытие" << endl;
    cout << "3 - Добавить к миссии перемещение" << endl;
    cout << "4 - Завершить миссию возвращением на базу" << endl;
    cout << "5 - Вернуться назад" << endl;

    int number = 0;
    cin >> number;

    switch(number)
    {
        case 1:
            AddDive(position - 1);
            number_methods_ = 10;
            break;
        case 2:
            AddLift(position - 1);
            number_methods_ = 10;
            break;
        case 3:
            AddMove(position - 1);
            number_methods_ = 10;
            break;
        case 4:
            AddReturn(position - 1);
            number_methods_ = 10;
            break;
        case 5:
            number_methods_ = 10;
            break;
        default:
            cout << "Ошибка ввода" << endl;
            number_methods_ = 10;
            break;
    }
}

void Menu::MoveNode()
{
    cout << endl;

    cout << "Введите первую позицию: " << endl;

    int position_1 = 0;
    cin >> position_1;

    if(position_1 > list_mission_.GetItems().size() || position_1 < 1)
    {
        cout << "В миссии нет задачи с таким номером" << endl;
        number_methods_ = 10;
        return;
    }

    cout << "Введите вторую позицию: " << endl;

    int position_2 = 0;
    cin >> position_2;

    if(position_2 > list_mission_.GetItems().size() || position_2 < 1)
    {
        cout << "В миссии нет задачи с таким номером" << endl;
        number_methods_ = 10;
        return;
    }

    list_mission_.Swap(position_1 - 1, position_2 - 1);

    number_methods_ = 10;
}

void Menu::ReplaceNode()
{
    cout << endl;

    cout << "Введите номер изменяемой задачи: " << endl;

    int position = 1;
    cin >> position;

    InsertNode(position);
    RmNode(position + 1);
}

void Menu::Safe()
{
    if(file_name.empty())
    {
        cout << endl;

        cout << "введите имя файла, без расширения" << endl;
        cin >> file_name;
    }

    ofstream file_with_mission;
    file_with_mission.open(file_name + ".txt");

    if(!file_with_mission.is_open())
    {
        cout << endl;

        cout << "не удалось открыть файл для записи";
    }
    else
    {
        Print(true, file_with_mission);
    }

    file_with_mission.close();
}