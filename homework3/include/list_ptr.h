#ifndef HOMEWORK3_LIST_PTR_H
#define HOMEWORK3_LIST_PTR_H

#include <algorithm>
#include <cassert>
#include <stdexcept>
#include <list>
#include <iterator>

template <typename T>
class PtrList {
public:
    PtrList() = default;
    PtrList(const PtrList& other);

    PtrList& operator=(const PtrList& rhs);

    ~PtrList();

public:
    std::list<T*>& GetItems() noexcept;
    std::list<T*> const& GetItems() const noexcept;

    void PushBack(T* back_ptr);
    void Insert(T* insertable_ptr, int number);
    void Erase(int number);
    void Swap(int number_1, int number_2);
    void Clear();

private:
    void swap(PtrList& other) noexcept {
        items_.swap(other.GetItems());
    }

private:
    std::list<T*> items_;
};

template <typename T>
PtrList<T>::PtrList(const PtrList& other)
{
    for(auto ptr_other : other.GetItems())
    {
        if(ptr_other == nullptr)
        {
            items_.push_back(nullptr);
            continue;
        }

        items_.push_back(new T(*ptr_other));
    }
}

template <typename T>
PtrList<T>& PtrList<T>::operator=(const PtrList& rhs)
{
    if (this != &rhs)
    {
        PtrList(rhs).swap(*this);
    }

    return *this;
}

template <typename T>
PtrList<T>::~PtrList()
{
    for(auto ptr : items_)
    {
        delete ptr;
    }
}

template <typename T>
std::list<T*>& PtrList<T>::GetItems() noexcept
{
    return items_;
}

template <typename T>
std::list<T*> const& PtrList<T>::GetItems() const noexcept {
    return items_;
}

template <typename T>
void PtrList<T>::PushBack(T* back_ptr) {
    items_.push_back(back_ptr);
}

template <typename T>
void PtrList<T>::Clear()
{
    for(auto ptr: items_)
    {
        delete(ptr);
    }

    items_.clear();
}

template<typename T>
void PtrList<T>::Erase(int number) {
    if(number >= items_.size())
    {
        throw std::invalid_argument("Невозможно удалить несуществующий элемент");
    }

    auto ptr = std::next(items_.begin(), number);

    delete *ptr;

    items_.erase(ptr);
}

template<typename T>
void PtrList<T>::Insert(T *insertable_ptr, int number) {
    if(number > (items_.size() + 1))
    {
        throw std::invalid_argument("Невозможно вставить на данную позицию");
    }

    auto ptr = std::next(items_.begin(), number);

    items_.insert(ptr, insertable_ptr);
}

template<typename T>
void PtrList<T>::Swap(int number_1, int number_2) {
    if(number_1 >= items_.size() || number_1 < 0 ||
       number_2 >= items_.size() || number_2 < 0)
    {
        throw std::invalid_argument("Неправильные позиции перемещаемых элементов");
    }

    auto ptr_1 = std::next(items_.begin(), number_1);
    auto ptr_2 = std::next(items_.begin(), number_2);
    std::iter_swap(ptr_1, ptr_2);
}

#endif //HOMEWORK3_LIST_PTR_H
