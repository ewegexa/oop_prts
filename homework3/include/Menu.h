#ifndef HOMEWORK3_MENU_H
#define HOMEWORK3_MENU_H


#include <string>
#include <iostream>

#include "list_ptr.h"
#include "mission_classes.h"

static const std::string VERSION("2.0");

class Menu {
public:
    Menu() = default;

    void Start();

private:
    void DefaultMenu();

    void AddDive(int position = -1);
    void AddLift(int position = -1);
    void AddMove(int position = -1);
    void AddReturn(int position = -1);

    void Print(bool return_menu = true, std::ostream& os = std::cout);

    void EndMenu();

    void ClearMission();

    void EditMenu();
    void RmNode(int position = -1);
    void InsertNode(int position = -1);
    void MoveNode();
    void ReplaceNode();
    void Safe();

    Mission* AddVerticalMove(int dive_ar_lift);
    void ConfirmAdd(Mission* new_node, const std::string& type_new_node, int position = -1);

private:
    int coordinates_start_[3] = {0, 0, 0};
    int number_methods_ = 1;
    PtrList<Mission> list_mission_;
    std::string file_name;
};

#endif //HOMEWORK3_MENU_H
