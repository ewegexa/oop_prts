#ifndef HOMEWORK3_MISSION_CLASSES_H
#define HOMEWORK3_MISSION_CLASSES_H

#include <ostream>
#include <utility>
#include <tuple>

const int WIDTH = 10'000;
const int LENGTH = 10'000;
const int DEPTH = 100;

class Mission {
public:
    Mission() = default;
    virtual ~Mission() = default;

    virtual bool IsEnd() const = 0;

public:
    virtual std::ostream& Print(std::ostream& os) const = 0;
};

enum class TypeSensors {
    PressureMeter,
    EchoSounder
};

enum class TypeDepth {
    Spiral,
    Vertical
};

class Dive: public Mission {
public:
    Dive(TypeSensors type_sensor, int depth, TypeDepth type_depth);

public:
    std::ostream& Print(std::ostream& os) const override;

    bool IsEnd() const override;

private:
    TypeSensors type_sensor_;
    int depth_;
    TypeDepth type_depth_;
};

class Lift: public Mission {
public:
    Lift(TypeSensors type_sensor, int depth, TypeDepth type_depth);

public:
    std::ostream& Print(std::ostream& os) const override;

    bool IsEnd() const override;

private:
    TypeSensors type_sensor_;
    int depth_;
    TypeDepth type_depth_;
};

enum class TypeIn {
    InPoint,
    AlongLine
};

enum class TypeOut {
    ConstDepth,
    ConstDistance
};

class Move: public Mission {
public:
    Move(int x, int y, TypeOut type_out, TypeIn type_in, double radius = 0.5);

public:
    std::ostream& Print(std::ostream& os) const override;

    bool IsEnd() const override;

private:
    std::pair<int, int> mv_coordinate_;
    TypeOut type_out_;
    TypeIn type_in_;
    double radius_;
};

class Return: public Mission {
public:
    Return() = default;

public:
    std::ostream& Print(std::ostream& os) const override;

    bool IsEnd() const override;
};

#endif //HOMEWORK3_MISSION_CLASSES_H
